import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedin = false;

  login() {
    console.log('login');
    this.isLoggedin = true;
  }

  logout() {
    console.log('logout');
    this.isLoggedin = false;
  }
}
