import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverviewRoutingModule } from './overview-routing.module';
import { OverviewComponent } from './overview.component';
import { SharedModule } from '../shared/shared.module';
import { DetailsModule } from './details/details.module';
import { DetailsComponent } from './details/details.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    OverviewRoutingModule,
    SharedModule,
    DetailsModule,
  ],
  declarations: [OverviewComponent, DetailsComponent]
})
export class OverviewModule { }
