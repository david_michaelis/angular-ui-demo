import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  id: number;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
   this.route.params.subscribe(params => {
       this.id = params.id;
    });
  }

}
