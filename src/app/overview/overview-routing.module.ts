import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview.component';
import { LoginGuard } from '../login/login.guard';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: 'overview',
    component: OverviewComponent,
    canActivate: [LoginGuard],
    children: [
      {
        path: 'details/:id',
        component: DetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OverviewRoutingModule { }
