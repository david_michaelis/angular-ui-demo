import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
  ],
  declarations: [ButtonComponent, HeaderComponent, FooterComponent, SideNavigationComponent],
  exports: [CommonModule, ButtonComponent, HeaderComponent, FooterComponent, SideNavigationComponent],
})
export class SharedModule { }
